﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CardScript : MonoBehaviour
{
    public Treasure treasure;
    public TextMeshProUGUI text;
    public bool cardFinish;

    public Vector3 targetPos;
    public float cardMoveSpeed;
    
    void Start()
    {
        targetPos = transform.position;
        HideText();
    }

    public void ShowText()
    {
        text.text = treasure.name;
    }

    public void HideText()
    {
        if (!cardFinish)
        {
            text.text = "";
        }
    }

    public void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetPos, cardMoveSpeed);
    }
}
