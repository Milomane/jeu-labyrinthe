﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LabyTileScript : MonoBehaviour
{
    public LabyTile labyTile;
    
    public TMP_Text treasureText;
    public bool up;
    public bool down;
    public bool left;
    public bool right;

    public string tilePlayerName;
    
    public SpriteRenderer spriteRenderer;

    public bool set;
    
    private float rotation;
    private float realRotation;
    public float spriteRotationSpeed = 0.01f;

    public bool playableTile;

    public Vector3 positionTarget;
    public float speed = 0.05f;

    private BoxCollider boxCollider;
    
    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        
        positionTarget = transform.position;

        // Init variables
        up = labyTile.up;
        down = labyTile.down;
        left = labyTile.left;
        right = labyTile.right;
        
        
        // Initialise la tile avec les trésors / Forme de la tile
        
        spriteRenderer.sprite = labyTile.tileSprite;
        if (labyTile.treasure != null)
        {
            treasureText.text = labyTile.treasure.name;
        }
        else
        {
            treasureText.text = "";
        }
        RotateSpriteRenderer(true);

        if (labyTile.playerName != "")
        {
            tilePlayerName = labyTile.playerName;
            spriteRenderer.material.color = labyTile.playerColor;
        }
    }

    void Update()
    {
        if (playableTile)
        {
            boxCollider.enabled = false;
            
            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                ChangeRotation(Input.GetAxis("Mouse ScrollWheel"));
            }
            
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = Camera.main.nearClipPlane;
            transform.position = Vector3.Lerp(transform.position, Camera.main.ScreenToWorldPoint(mousePos), 0.3f);
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
        else
        {
            boxCollider.enabled = true;
            transform.position = Vector3.Lerp(transform.position, positionTarget, speed);
        }
        RotateSpriteRenderer(false);
    }

    public bool setTile(LabyTile tile)
    {
        if (set)
        {
            return false;
        }
        else
        {
            labyTile = tile;
            
            // Init variables
            up = labyTile.up;
            down = labyTile.down;
            left = labyTile.left;
            right = labyTile.right;
        
        
            // Initialise la tile avec les trésors / Forme de la tile
        
            spriteRenderer.sprite = labyTile.tileSprite;
            if (labyTile.treasure != null)
            {
                treasureText.text = labyTile.treasure.name;
            }
            else
            {
                treasureText.text = "";
            }
            RotateSpriteRenderer(true);

            if (labyTile.playerName != "")
            {
                tilePlayerName = labyTile.playerName;
                spriteRenderer.material.color = labyTile.playerColor;
            }

            set = true;
            return true;
        }
    }

    public void RotateSpriteRenderer(bool instant)
    {
        // Compte le nombre de sortie de la case (2 ou 3)
        int count = 0;
        if (up) count++;
        if (down) count++;
        if (left) count++;
        if (right) count++;

        // Verifie les possibilité des case a 2 sortie et définie l'objectif de rotation en fonction
        if (count == 2)
        {
            // Corner tile
            if (down && right)
            {
                rotation = 0;
            }

            if (up && right)
            {
                rotation = 90;
            }

            if (up && left)
            {
                rotation = 180;
            }

            if (down && left)
            {
                rotation = 270;
            }

            // Line tile
            if (up && down)
            {
                rotation = 0;
            }

            if (left && right)
            {
                rotation = 90;
            }
        }
        else // Verifie les possibilité des case a 3 sortie et définie l'objectif de rotation en fonction
        {
            // Three way tile
            if (left && down && right)
            {
                rotation = 0;
            }

            if (up && down && right)
            {
                rotation = 90;
            }

            if (left && up && right)
            {
                rotation = 180;
            }

            if (left && down && up)
            {
                rotation = 270;
            }
        }
        // Smooth la rotation
        realRotation = Mathf.Lerp(realRotation, rotation, spriteRotationSpeed);
        
        // Check si la rotation doit être instantané
        if (instant) realRotation = rotation;
        
        // Applique la rotation
        spriteRenderer.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, realRotation));
        
    }

    public void ChangeRotation(float axis)
    {
        bool nextLeft;
        bool nextDown;
        bool nextRight;
        bool nextUp;
        
        // En fonction de la direction du scroll de la souris la tile tourne dans un sens ou dans l'autre
        if (axis > 0)
        {
            nextLeft = up;
            nextDown = left;
            nextRight = down;
            nextUp = right;
        }
        else
        {
            nextRight = up;
            nextDown = right;
            nextLeft = down;
            nextUp = left;
        }
        
        // Applique la rotation
        up = nextUp;
        down = nextDown;
        left = nextLeft;
        right = nextRight;
    }
}
