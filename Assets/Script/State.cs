﻿using System.Collections;

namespace DefaultNamespace
{
    public abstract class State
    {
        protected GameplayManager GameplayManager;

        protected State(GameplayManager gameplayManager)
        {
            GameplayManager = gameplayManager;
        }


        public virtual IEnumerator Start()
        {
            yield break;
        }
        
        public virtual IEnumerator PlayerTile()
        {
            yield break;
        }
        
        public virtual IEnumerator PlayerMove()
        {
            yield break;
        }
        public virtual IEnumerator RestartGame()
        {
            yield break;
        }
    }
}