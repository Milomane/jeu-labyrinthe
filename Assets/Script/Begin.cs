﻿using System.Collections;
using System.IO;
using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class Begin : State
    {
        public Begin(GameplayManager gameplayManager) : base(gameplayManager)
        {
        }

        public override IEnumerator Start()
        {
            GameplayManager.startButton.SetActive(false);
            
            // Distribution des cartes
            GameplayManager.CardDistribution();

            int startPlayer = Random.Range(0, GameplayManager.playerCount-1);
            GameplayManager.currentPlayerTurn = startPlayer;
            
            GameplayManager.infoPanel.SetActive(true);
            GameplayManager.infoPanel.GetComponentInChildren<TMP_Text>().text = ($"{GameplayManager.player[startPlayer].playerName} player Start");

            yield return new WaitForSeconds(2f);
            
            GameplayManager.infoPanel.SetActive(false);
            
            GameplayManager.SetState(new PlayerTurn(GameplayManager));
        }
    }
}