﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

public class Labyrinthe : MonoBehaviour
{
    public LabyArray[] labyArray2d;

    public GameObject tileInHand;
    public Transform tileInHandWaitPos;

    public List<Deck> deck;
    public List<LabyTile> trueDeck;

    public GameObject[] buttons;

    private GameplayManager gameplayManager;

    public GameObject exception;
    
    [System.Serializable]
    public class LabyArray
    {
         public GameObject[] labyArray;
    }
    
    [System.Serializable]
    public class Deck
    {
        public LabyTile labyTile;
        public int count;
    }

    public void Start()
    {
        gameplayManager = FindObjectOfType<GameplayManager>();

        // Auto generate labyrinthe
        GenerateLabyrinthe();
    }

    public void MoveLabyrinthe(ButtonData buttonData)
    {
        GameObject newTileInHand = null;
        int row = buttonData.row;
        int column = buttonData.column;
        bool increase = buttonData.increase;
        
        gameplayManager.OnPlayerPlaceTile();
        exception = EventSystem.current.currentSelectedGameObject.GetComponent<ArrowScript>().opositeButton;
        
        // Check if need to move row
        if (row != 0)
        {
            Debug.Log("Ligne " + row + " bouge");
            
            // Check if need to increase or decrease arrayValue
            if (increase)
            {
                Debug.Log("Vers la droite");
                    // Place tile in hand in array
                newTileInHand = labyArray2d[row].labyArray[6];
                
                // Move the raw in array
                for (int i = 6; i > 0; i--)
                {
                    labyArray2d[row].labyArray[i] = labyArray2d[row].labyArray[i-1];

                    // Change tile position
                    labyArray2d[row].labyArray[i].GetComponent<LabyTileScript>().positionTarget = labyArray2d[row].labyArray[i].GetComponent<LabyTileScript>().positionTarget + Vector3.right;
                }
                
                tileInHand.GetComponent<LabyTileScript>().positionTarget = new Vector3(-3, 3 - row, 0);
                labyArray2d[row].labyArray[0] = tileInHand;
            }
            else
            {
                Debug.Log("Vers la gauche");
                // Place tile in hand in array
                newTileInHand = labyArray2d[row].labyArray[0];
                
                // Move the raw in array
                for (int i = 0; i < 6; i++)
                {
                    labyArray2d[row].labyArray[i] = labyArray2d[row].labyArray[i+1];
                    
                    // Change tile position
                    labyArray2d[row].labyArray[i].GetComponent<LabyTileScript>().positionTarget = labyArray2d[row].labyArray[i].GetComponent<LabyTileScript>().positionTarget + Vector3.left;
                }
                
                tileInHand.GetComponent<LabyTileScript>().positionTarget = new Vector3(3, 3 - row, 0);
                labyArray2d[row].labyArray[6] = tileInHand;
            }
        }
        
        // Check if need to move column
        if (column != 0)
        {
            Debug.Log("Collone " + column + " bouge");
            
            if (increase)
            {
                Debug.Log("Vers le haut");
                // Place tile in hand in array
                newTileInHand = labyArray2d[0].labyArray[column];
                
                // Move the column in array
                for (int i = 0; i < 6; i++)
                {
                    labyArray2d[i].labyArray[column] = labyArray2d[i+1].labyArray[column];
                    
                    // Change tile position
                    labyArray2d[i].labyArray[column].GetComponent<LabyTileScript>().positionTarget = labyArray2d[i].labyArray[column].GetComponent<LabyTileScript>().positionTarget + Vector3.up;
                }
                
                tileInHand.GetComponent<LabyTileScript>().positionTarget = new Vector3(column - 3, -3, 0);
                labyArray2d[6].labyArray[column] = tileInHand;
            }
            else
            {
                Debug.Log("Vers le bas");
                // Place tile in hand
                newTileInHand = labyArray2d[6].labyArray[column];
                
                // Move the column in array
                for (int i = 6; i > 0; i--)
                {
                    labyArray2d[i].labyArray[column] = labyArray2d[i-1].labyArray[column];
                    
                    // Change tile position
                    labyArray2d[i].labyArray[column].GetComponent<LabyTileScript>().positionTarget = labyArray2d[i].labyArray[column].GetComponent<LabyTileScript>().positionTarget + Vector3.down;
                }
                
                tileInHand.GetComponent<LabyTileScript>().positionTarget = new Vector3(column - 3, 3, 0);
                labyArray2d[0].labyArray[column] = tileInHand;
            }
        }

        tileInHand = newTileInHand;
        tileInHand.GetComponent<LabyTileScript>().positionTarget = tileInHandWaitPos.position;
    }

    public void GenerateLabyrinthe()
    {
        foreach (Deck rd in deck)
        {
            for (int i = 0; i < rd.count; i++)
            {
                trueDeck.Add(rd.labyTile);
            }
        }
        
        Debug.Log("Generate");
        
        for (int i = 0; i < 7; i++)
        {
            for (int y = 0; y < 7; y++)
            {
                int r = Random.Range(0, trueDeck.Count-1);
                if (trueDeck.Count > 0)
                {
                    if (labyArray2d[y].labyArray[i].GetComponent<LabyTileScript>().setTile(trueDeck[r]))
                    {
                        int rr = Random.Range(0, 3);
                        for (int u = 0; u < rr; u++)
                        {
                            labyArray2d[y].labyArray[i].GetComponent<LabyTileScript>().ChangeRotation(1);
                        }
                        labyArray2d[y].labyArray[i].GetComponent<LabyTileScript>().RotateSpriteRenderer(true);
                    
                        trueDeck.RemoveAt(r);
                    }
                }
            }
        }
    }

    public void activeAllButton()
    {
        foreach (var g in buttons)
        {
            if (g != exception) g.SetActive(true);
        }
    }

    public void deactiveAllButton()
    {
        foreach (var g in buttons)
        {
            g.SetActive(false);
        }
    }
}
