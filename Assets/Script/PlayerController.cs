﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public string playerName;
    public float moveSpeed = 2;
    public PlayerCardScript cardScript;

    public Vector2 positionOnArray;
    public LabyTileScript onTile;

    public Labyrinthe labyrinthe;
    public PlayerController[] otherPlayers;
    public Vector3 shiftVector;
    
    public GameObject trailObject;
    public float trailSpeed = 25;
    public float trailSpawnTime = 0.4f;
    private float trailTimer;

    private LabyTileScript mouseTileTarget;
    public bool select;
    public List<GameObject> bufferTileList = new List<GameObject>();
    public List<GameObject> list = new List<GameObject>();
    private bool reachTile = true;
    private Vector3 theoreticalPosition;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (labyrinthe.tileInHand == onTile.gameObject)
        {
            if (positionOnArray.x == 0)
            {
                onTile = labyrinthe.labyArray2d[Mathf.RoundToInt(positionOnArray.y)].labyArray[6].GetComponent<LabyTileScript>();
            } 
            else if (positionOnArray.x == 6)
            {
                onTile = labyrinthe.labyArray2d[Mathf.RoundToInt(positionOnArray.y)].labyArray[0].GetComponent<LabyTileScript>();
            } 
            else if (positionOnArray.y == 0)
            {
                onTile = labyrinthe.labyArray2d[6].labyArray[Mathf.RoundToInt(positionOnArray.x)].GetComponent<LabyTileScript>();
            }
            else
            {
                onTile = labyrinthe.labyArray2d[0].labyArray[Mathf.RoundToInt(positionOnArray.x)].GetComponent<LabyTileScript>();
            }
        }
        
        // Move player in array and space
        float y = -(onTile.positionTarget.y - 3);
        float x = onTile.positionTarget.x + 3;
        positionOnArray = new Vector2(x, y);
        
        //transform.position = Vector3.Lerp(transform.position, new Vector3(onTile.positionTarget.x, onTile.positionTarget.y, 0), moveSpeed);
        
        
        // detect tile under mouse
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.GetComponent<LabyTileScript>())
            {
                mouseTileTarget = hit.collider.GetComponent<LabyTileScript>();
            }
            else
            {
                mouseTileTarget = null;
            }
        }
        else
        {
            mouseTileTarget = null;
        }
        
        // Recursive on when mouse on tile
        if (select && mouseTileTarget != null)
        {
            // Reset buffer list
            bufferTileList = new List<GameObject>();
            list.Clear();
            
            // Start recursive and get PathList
            list = RecursiveTileDetect(Mathf.RoundToInt(positionOnArray.x), Mathf.RoundToInt(positionOnArray.y), list, 0);
            
            // Draw line
            if (trailTimer <= 0)
            {
                GameObject trail = Instantiate(trailObject, transform.position, Quaternion.identity);
                StartCoroutine(FollowPath(trail, list, trailSpeed, true));
                trailTimer = trailSpawnTime;
            }
            else
            {
                trailTimer -= Time.deltaTime;
            }

            // If path is possible
            if (list.Count > 0)
            {
                // If player click on left mouse
                if (reachTile)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        // Set tile the current tile of player
                        onTile = list[list.Count - 1].GetComponent<LabyTileScript>();
                        reachTile = false;
                        StartCoroutine(FollowPath(gameObject, list, moveSpeed, false));
                    }
                }
            }
        }
        else
        {
            trailTimer = 0;
            list.Clear();
        }

        if (reachTile)
        {
            transform.position = onTile.transform.position;
        }
            ShiftPlayerOnSameTile();
    }

    public List<GameObject> RecursiveTileDetect(int x, int y, List<GameObject> tileList, int recursiveCount)
    {
        recursiveCount++;
        if (recursiveCount > 50) return tileList;

        LabyTileScript actualTileScript = labyrinthe.labyArray2d[y].labyArray[x].GetComponent<LabyTileScript>();
        
        // Add object to buffer list to not double check it
        bufferTileList.Add(actualTileScript.gameObject);
        // Add Object to tile list
        tileList.Add(actualTileScript.gameObject);

        // return list if this tile is objectif
        if (tileList.Contains(mouseTileTarget.gameObject)) return tileList;
        
        
        // Check if access to next tile is possible, if yes, recall the recursive
        
        if (y > 0)
        {
            if (actualTileScript.up && labyrinthe.labyArray2d[y - 1].labyArray[x].GetComponent<LabyTileScript>().down && !bufferTileList.Contains(labyrinthe.labyArray2d[y - 1].labyArray[x]))
            {
                tileList = RecursiveTileDetect(x, y - 1, tileList, recursiveCount);
                if (tileList.Contains(mouseTileTarget.gameObject)) return tileList;
            }
        }

        if (y < 6)
        {
            if (actualTileScript.down && labyrinthe.labyArray2d[y + 1].labyArray[x].GetComponent<LabyTileScript>().up && !bufferTileList.Contains(labyrinthe.labyArray2d[y + 1].labyArray[x]))
            {
                tileList = RecursiveTileDetect(x, y + 1, tileList, recursiveCount);
                if (tileList.Contains(mouseTileTarget.gameObject)) return tileList;
            }
        }

        if (x > 0)
        {
            if (actualTileScript.left && labyrinthe.labyArray2d[y].labyArray[x - 1].GetComponent<LabyTileScript>().right && !bufferTileList.Contains(labyrinthe.labyArray2d[y].labyArray[x - 1]))
            {
                tileList = RecursiveTileDetect(x - 1, y, tileList, recursiveCount);
                if (tileList.Contains(mouseTileTarget.gameObject)) return tileList;
            }
        }
        
        if (x < 6)
        {
            if (actualTileScript.right && labyrinthe.labyArray2d[y].labyArray[x + 1].GetComponent<LabyTileScript>().left && !bufferTileList.Contains(labyrinthe.labyArray2d[y].labyArray[x + 1]))
            {
                tileList = RecursiveTileDetect(x + 1, y, tileList, recursiveCount);
                if (tileList.Contains(mouseTileTarget.gameObject)) return tileList;
            }
        }
        
        
        tileList.RemoveAt(tileList.Count - 1);
        
        return tileList;
    }

    private void ShiftPlayerOnSameTile()
    {
        foreach (var other in otherPlayers)
        {
            if (onTile == other.onTile)
            {
                if (reachTile)
                {
                    transform.position = theoreticalPosition + shiftVector;
                }
            }
        }
    }

    private IEnumerator FollowPath(GameObject o, List<GameObject> pathList, float speed, bool destroyAtEnd)
    {
        bool objectif = false;
        int index = 1;
        
        List<GameObject> pathListN = new List<GameObject>();
        foreach (var pathObject in pathList)
        {
            pathListN.Add(pathObject);
        }
        
        int pathCount = pathListN.Count;

        while (index <= pathCount - 1)
        {
            Debug.Log(index + "   " + pathCount + "   " + pathListN.Count); 
            
            o.transform.position = Vector2.MoveTowards(o.transform.position, pathListN[index].transform.position, speed * Time.deltaTime);
            
            if (o.transform.position == pathListN[index].transform.position)
            {
                index++;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }

        if (destroyAtEnd)
        {
            Destroy(o, 0.5f);
        }

        if (o == gameObject)
        {
            theoreticalPosition = transform.position;
            reachTile = true;
        }
        
        Debug.Log("Index : " + index + " index lenght : " + (pathList.Count - 1));
    }

    public bool victoryConditon()
    {
        if (onTile.name == playerName && cardScript.cardList.Count == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckCardOnTile()
    {
        if (cardScript.cardList.Count > 0)
        {
            if (onTile.labyTile.treasure == cardScript.cardList[cardScript.cardList.Count - 1].treasure)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    private void OnDrawGizmos()
    {
        for (int i = 1; i < list.Count; i++)
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(list[i-1].gameObject.transform.position, list[i].gameObject.transform.position);
        }
    }
}
