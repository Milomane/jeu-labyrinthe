﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameplayManager : StateMachine
{
    public GameObject startButton;
    public GameObject infoPanel;
    
    public int playerCount = 4;
    public List<PlayerController> player;
    public GameObject playerButton;

    public TextMeshProUGUI playerTurnInfo;

    public Labyrinthe labyrinthe;

    public int currentPlayerTurn;

    public List<Treasure> treasures;

    public bool win = false;
    

    public void OnStartButton()
    {
        SetState(new Begin(this));
    }

    public void OnPlayerPlaceTile()
    {
        StartCoroutine(State.PlayerTile());
    }

    public void OnPlayerFinishButton()
    {
        StartCoroutine(State.PlayerMove());
    }

    public void OnRestartGame()
    {
        if (win)
        {
            StartCoroutine(State.RestartGame());
        }
    }

    public void CardDistribution()
    {
        int cardByPlayer = treasures.Count / playerCount;
        
        for (int i = 0; i < playerCount; i++)
        {
            for (int y = 0; y < cardByPlayer; y++)
            {
                int r = Random.Range(0, treasures.Count);
                player[i].cardScript.GenerateCard(treasures[r]);
                treasures.RemoveAt(r);
            }
        }
    }

    public void HideAllCard()
    {
        foreach (var p in player)
        {
            p.cardScript.HideCards();
        }
    }

    public void Update()
    {
        if (Input.anyKeyDown)
        {
            OnRestartGame();
        }
    }
}
