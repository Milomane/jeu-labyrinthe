﻿using System.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public class PlayerTurn : State
    {
        public PlayerTurn(GameplayManager gameplayManager) : base(gameplayManager)
        {
        }

        public PlayerController player;
        
        public override IEnumerator Start()
        {
            player = GameplayManager.player[GameplayManager.currentPlayerTurn];
            
            Debug.Log($"player {player.playerName} TurnStart");
            GameplayManager.playerTurnInfo.text = ($"{player.playerName} turn");
            GameplayManager.playerTurnInfo.color = player.GetComponent<SpriteRenderer>().color;
            
            GameplayManager.HideAllCard();
            PlayerCardScript cardScript = player.cardScript;
            if (cardScript.cardList.Count > 0)
            cardScript.cardList[cardScript.cardList.Count-1].ShowText();
            
            GameplayManager.labyrinthe.activeAllButton();
            GameplayManager.labyrinthe.tileInHand.GetComponent<LabyTileScript>().playableTile = true;
            yield break;
        }

        public override IEnumerator PlayerTile()
        {
            GameplayManager.labyrinthe.deactiveAllButton();
            GameplayManager.labyrinthe.tileInHand.GetComponent<LabyTileScript>().playableTile = false;
            
            yield return new WaitForSeconds(1f);
            
            player.select = true;
            GameplayManager.playerButton.SetActive(true);
        }
        
        public override IEnumerator PlayerMove()
        {
            GameplayManager.playerButton.SetActive(false);
            player.select = false;

            if (player.CheckCardOnTile())
            {
                player.cardScript.ValideCard();
            }

            if (player.victoryConditon())
            {
                GameplayManager.SetState(new Won(GameplayManager));
            }
            else
            {
                yield return new WaitForSeconds(1f);

                if (GameplayManager.currentPlayerTurn < GameplayManager.playerCount-1)
                {
                    GameplayManager.currentPlayerTurn++;
                }
                else
                {
                    GameplayManager.currentPlayerTurn = 0;
                }
                GameplayManager.SetState(new PlayerTurn(GameplayManager));
            }
        }
    }
}