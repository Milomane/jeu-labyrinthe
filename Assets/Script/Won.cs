﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class Won : State
    {
        public Won(GameplayManager gameplayManager) : base(gameplayManager)
        {
        }
        
        public override IEnumerator Start()
        {
            GameplayManager.playerTurnInfo.text = ($"{GameplayManager.player[GameplayManager.currentPlayerTurn].playerName} WIN");
            GameplayManager.win = true;
            yield break;
        }

        public override IEnumerator RestartGame()
        {
            GameplayManager.playerTurnInfo.text = ("Game restart in 3");
            yield return new WaitForSeconds(1);
            GameplayManager.playerTurnInfo.text = ("Game restart in 2");
            yield return new WaitForSeconds(1);
            GameplayManager.playerTurnInfo.text = ("Game restart in 1");
            yield return new WaitForSeconds(1);
            
            Application.LoadLevel(Application.loadedLevel);
        }
    }
}