﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LabyTile : ScriptableObject
{
    // Déclare les variables dont on a besoin pour les tiles
    
    public bool up;
    public bool down;
    public bool left;
    public bool right;

    public Sprite tileSprite;
    
    public Treasure treasure;

    public string playerName;
    public Color playerColor;
}
