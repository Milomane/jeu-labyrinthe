﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ButtonData : ScriptableObject
{
    public int row;
    public int column;
    public bool increase;
}
