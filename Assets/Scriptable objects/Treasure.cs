﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Treasure : ScriptableObject
{
    // Declare les variables dont ont a besoin pour les objets
    
    public string name;
}
