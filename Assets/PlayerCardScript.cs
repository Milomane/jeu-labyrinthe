﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCardScript : MonoBehaviour
{
    public GameObject cardPrefab;
    public List<CardScript> cardList;

    public Vector3 cardJumpDistance = new Vector3(1,0,0);

    public Color cardColor;

    public void GenerateCard(Treasure t)
    {
        GameObject o = Instantiate(cardPrefab, transform);
        o.GetComponent<CardScript>().treasure = t;
        o.GetComponent<SpriteRenderer>().color = cardColor;
        
        cardList.Add(o.GetComponent<CardScript>()); 
    }

    public void HideCards()
    {
        foreach (var card in cardList)
        {
            card.HideText();
        }
    }

    public void ValideCard()
    {
        cardList[cardList.Count - 1].cardFinish = true;
        cardList[cardList.Count - 1].targetPos += cardJumpDistance * (cardList.Count - 1);
        cardList.RemoveAt(cardList.Count - 1);
    }
}
